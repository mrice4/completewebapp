const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors")

//app.set("port", 8080);
app.set("port", 8080);
app.use(bodyParser.json({type:"application/json"}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

const Pool = require("pg").Pool;
const config = {
  host: "localhost",
  user: "food_nutrition",
  password: "Th3W0rmTurn5",
  database: "food_nutrition"
};

const pool = new Pool(config);

app.get("/api/list-entries", async(req, res) => {
  try {
      const value = req.query.v
      const template = "SELECT * FROM entries WHERE description ILIKE $1 LIMIT 10";
      const response = await pool.query(template, [value+'%']);
      res.json(response.rows);
  } catch (err) {
    console.log(err);
    res.json({error: err, status: 'Unforseen error occured, contact builder'});
  }
});

app.listen(app.get("port"), () => {
  console.log(`Find the server at http://localhost:${app.get("port")}`);
});
