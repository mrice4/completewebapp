const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #000000',
  backgroundColor: '#f5d6ff',

}

export default function Layout(props) {
  return (
    <div style={layoutStyle}>
      {props.children}
    </div>
  )
}
