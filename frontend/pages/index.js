require ("isomorphic-fetch")
import React from "react";
import {getInfo} from '../lib/utils.js';
import Layout from '../components/Layout.js';

const textStyle = {
  fontFamily: "Arial"
}

const imgStyle = {
  height: "200px",
  marginTop: "20px"
}

const tableStyle = {
  margin: "0 auto",
  width: "100%",
  align: "center",
  padding: "5px",
  borderCollapse: "separate",
  borderBottom: "1px solid black"

}

const returnedTextStyle = {
    fontFamily: "Avantgarde, sans-serif"
}

  class FindCompnent extends React.Component {
    constructor(props) {
      super(props);
      this.state={search: ""}
  }

  handleUpdate(evt){
    this.setState({search: evt.target.value});
  }

  async handleSearch(evt) {
    this.handleUpdate(evt);
    const value = await getInfo(evt.target.value);
    this.setState({value});
  }

  render() {
    return (
      <Layout>
      <div style={{ margin: "auto auto", width: "600px", textAlign: "center"}}>
        <h2>Food Search</h2>
        <img src="/static/Bounty.png" alt="someFood" className="App-logo" style={imgStyle}/>
        <p style={textStyle}><input type='text' value={this.state.search} onChange={this.handleSearch.bind(this)} /></p>
        {("value" in this.state && this.state.value.length > 0) ? <div>
            <br />
            <table style={tableStyle}>
            <thead>
            <th>Description</th>
            <th>KCal</th>
            <th>Protein</th>
            <th>Total Fat</th>
            <th>Carbs</th>
            </thead>
            <tbody>
            {this.state.value.map(function(item) {
              return(
                <tr>
                <td>{item.description}</td>
                <td>{item.kcal}</td>
                <td>{item.protein_g}</td>
                <td>{(item.fa_sat_g + item.fa_mono_g + item.fa_poly_g).toFixed(2)}</td>
                <td>{item.carbohydrate_g}</td>
              </tr>
              )
              })
            }
            </tbody>
            </table>
          </div> : null}

        <style jsx>{`

          .description {
            font-family: "Arial";
            font-size: "10px";
          }

          ul {
            padding: 0;
          }

          li {
            list-style: none;
            margin: 5px 0;
          }
          a {
            text-decoration: none;
            color: blue;
          }

          a:hover {
            opacity: 0.6;
          }
        `}</style>
      </div>
      </Layout>
    );
  }
}

export default FindCompnent;
