module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Layout.js":
/*!******************************!*\
  !*** ./components/Layout.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Layout; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/components/Layout.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #000000',
  backgroundColor: '#f5d6ff'
};
function Layout(props) {
  return __jsx("div", {
    style: layoutStyle,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, props.children);
}

/***/ }),

/***/ "./lib/utils.js":
/*!**********************!*\
  !*** ./lib/utils.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

var _bluebird = _interopRequireDefault(__webpack_require__(/*! bluebird */ "bluebird"));

__webpack_require__(/*! isomorphic-fetch */ "isomorphic-fetch");

function handleError(err) {
  console.warn(err);
  return null;
}

;

function getNutritionalInfo(value) {
  return fetch(`http://localhost:8000/api/list-entries?v=${value}`).then(function (resp) {
    return resp.json();
  });
}

module.exports = {
  getInfo: function (value) {
    if (value != "") {
      return getNutritionalInfo(value).catch(handleError);
    } else {
      const arr = [];
      return arr;
    }
  }
};

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../lib/utils.js */ "./lib/utils.js");
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_lib_utils_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_Layout_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Layout.js */ "./components/Layout.js");
var _jsxFileName = "/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

__webpack_require__(/*! isomorphic-fetch */ "isomorphic-fetch");




const textStyle = {
  fontFamily: "Arial"
};
const imgStyle = {
  height: "200px",
  marginTop: "20px"
};
const tableStyle = {
  margin: "0 auto",
  width: "100%",
  align: "center",
  padding: "5px",
  borderCollapse: "separate",
  borderBottom: "1px solid black"
};
const returnedTextStyle = {
  fontFamily: "Avantgarde, sans-serif"
};

class FindCompnent extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ""
    };
  }

  handleUpdate(evt) {
    this.setState({
      search: evt.target.value
    });
  }

  async handleSearch(evt) {
    this.handleUpdate(evt);
    const value = await Object(_lib_utils_js__WEBPACK_IMPORTED_MODULE_2__["getInfo"])(evt.target.value);
    this.setState({
      value
    });
  }

  render() {
    return __jsx(_components_Layout_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }, __jsx("div", {
      style: {
        margin: "auto auto",
        width: "600px",
        textAlign: "center"
      },
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: this
    }, __jsx("h2", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49
      },
      __self: this
    }, "Food Search"), __jsx("img", {
      src: "/static/Bounty.png",
      alt: "someFood",
      style: imgStyle,
      className: "jsx-1455412518" + " " + "App-logo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: this
    }), __jsx("p", {
      style: textStyle,
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    }, __jsx("input", {
      type: "text",
      value: this.state.search,
      onChange: this.handleSearch.bind(this),
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: this
    })), "value" in this.state && this.state.value.length > 0 ? __jsx("div", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52
      },
      __self: this
    }, __jsx("br", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }), __jsx("table", {
      style: tableStyle,
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, __jsx("thead", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, __jsx("th", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: this
    }, "Description"), __jsx("th", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }, "KCal"), __jsx("th", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    }, "Protein"), __jsx("th", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }, "Total Fat"), __jsx("th", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: this
    }, "Carbs")), __jsx("tbody", {
      className: "jsx-1455412518",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }, this.state.value.map(function (item) {
      return __jsx("tr", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        },
        __self: this
      }, __jsx("td", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        },
        __self: this
      }, item.description), __jsx("td", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        },
        __self: this
      }, item.kcal), __jsx("td", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, item.protein_g), __jsx("td", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }, (item.fa_sat_g + item.fa_mono_g + item.fa_poly_g).toFixed(2)), __jsx("td", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, item.carbohydrate_g));
    })))) : null, __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
      id: "1455412518",
      __self: this
    }, ".description.jsx-1455412518{font-family:\"Arial\";font-size:\"10px\";}ul.jsx-1455412518{padding:0;}li.jsx-1455412518{list-style:none;margin:5px 0;}a.jsx-1455412518{-webkit-text-decoration:none;text-decoration:none;color:blue;}a.jsx-1455412518:hover{opacity:0.6;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQThFb0IsQUFJaUMsQUFLVixBQUlNLEFBSUssQUFLVCxVQVpkLEVBYUEsSUFUZSxJQVRJLFNBVW5CLFFBVEEsYUFZYSxXQUNiIiwiZmlsZSI6Ii9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsicmVxdWlyZSAoXCJpc29tb3JwaGljLWZldGNoXCIpXHJcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtnZXRJbmZvfSBmcm9tICcuLi9saWIvdXRpbHMuanMnO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gJy4uL2NvbXBvbmVudHMvTGF5b3V0LmpzJztcclxuXHJcbmNvbnN0IHRleHRTdHlsZSA9IHtcclxuICBmb250RmFtaWx5OiBcIkFyaWFsXCJcclxufVxyXG5cclxuY29uc3QgaW1nU3R5bGUgPSB7XHJcbiAgaGVpZ2h0OiBcIjIwMHB4XCIsXHJcbiAgbWFyZ2luVG9wOiBcIjIwcHhcIlxyXG59XHJcblxyXG5jb25zdCB0YWJsZVN0eWxlID0ge1xyXG4gIG1hcmdpbjogXCIwIGF1dG9cIixcclxuICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgYWxpZ246IFwiY2VudGVyXCIsXHJcbiAgcGFkZGluZzogXCI1cHhcIixcclxuICBib3JkZXJDb2xsYXBzZTogXCJzZXBhcmF0ZVwiLFxyXG4gIGJvcmRlckJvdHRvbTogXCIxcHggc29saWQgYmxhY2tcIlxyXG5cclxufVxyXG5cclxuY29uc3QgcmV0dXJuZWRUZXh0U3R5bGUgPSB7XHJcbiAgICBmb250RmFtaWx5OiBcIkF2YW50Z2FyZGUsIHNhbnMtc2VyaWZcIlxyXG59XHJcblxyXG4gIGNsYXNzIEZpbmRDb21wbmVudCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG4gICAgICBzdXBlcihwcm9wcyk7XHJcbiAgICAgIHRoaXMuc3RhdGU9e3NlYXJjaDogXCJcIn1cclxuICB9XHJcblxyXG4gIGhhbmRsZVVwZGF0ZShldnQpe1xyXG4gICAgdGhpcy5zZXRTdGF0ZSh7c2VhcmNoOiBldnQudGFyZ2V0LnZhbHVlfSk7XHJcbiAgfVxyXG5cclxuICBhc3luYyBoYW5kbGVTZWFyY2goZXZ0KSB7XHJcbiAgICB0aGlzLmhhbmRsZVVwZGF0ZShldnQpO1xyXG4gICAgY29uc3QgdmFsdWUgPSBhd2FpdCBnZXRJbmZvKGV2dC50YXJnZXQudmFsdWUpO1xyXG4gICAgdGhpcy5zZXRTdGF0ZSh7dmFsdWV9KTtcclxuICB9XHJcblxyXG4gIHJlbmRlcigpIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxMYXlvdXQ+XHJcbiAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luOiBcImF1dG8gYXV0b1wiLCB3aWR0aDogXCI2MDBweFwiLCB0ZXh0QWxpZ246IFwiY2VudGVyXCJ9fT5cclxuICAgICAgICA8aDI+Rm9vZCBTZWFyY2g8L2gyPlxyXG4gICAgICAgIDxpbWcgc3JjPVwiL3N0YXRpYy9Cb3VudHkucG5nXCIgYWx0PVwic29tZUZvb2RcIiBjbGFzc05hbWU9XCJBcHAtbG9nb1wiIHN0eWxlPXtpbWdTdHlsZX0vPlxyXG4gICAgICAgIDxwIHN0eWxlPXt0ZXh0U3R5bGV9PjxpbnB1dCB0eXBlPSd0ZXh0JyB2YWx1ZT17dGhpcy5zdGF0ZS5zZWFyY2h9IG9uQ2hhbmdlPXt0aGlzLmhhbmRsZVNlYXJjaC5iaW5kKHRoaXMpfSAvPjwvcD5cclxuICAgICAgICB7KFwidmFsdWVcIiBpbiB0aGlzLnN0YXRlICYmIHRoaXMuc3RhdGUudmFsdWUubGVuZ3RoID4gMCkgPyA8ZGl2PlxyXG4gICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgPHRhYmxlIHN0eWxlPXt0YWJsZVN0eWxlfT5cclxuICAgICAgICAgICAgPHRoZWFkPlxyXG4gICAgICAgICAgICA8dGg+RGVzY3JpcHRpb248L3RoPlxyXG4gICAgICAgICAgICA8dGg+S0NhbDwvdGg+XHJcbiAgICAgICAgICAgIDx0aD5Qcm90ZWluPC90aD5cclxuICAgICAgICAgICAgPHRoPlRvdGFsIEZhdDwvdGg+XHJcbiAgICAgICAgICAgIDx0aD5DYXJiczwvdGg+XHJcbiAgICAgICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgICAgIDx0Ym9keT5cclxuICAgICAgICAgICAge3RoaXMuc3RhdGUudmFsdWUubWFwKGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAgICAgICByZXR1cm4oXHJcbiAgICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2l0ZW0uZGVzY3JpcHRpb259PC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZD57aXRlbS5rY2FsfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2l0ZW0ucHJvdGVpbl9nfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+eyhpdGVtLmZhX3NhdF9nICsgaXRlbS5mYV9tb25vX2cgKyBpdGVtLmZhX3BvbHlfZykudG9GaXhlZCgyKX08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPntpdGVtLmNhcmJvaHlkcmF0ZV9nfTwvdGQ+XHJcbiAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICA8L3RhYmxlPlxyXG4gICAgICAgICAgPC9kaXY+IDogbnVsbH1cclxuXHJcbiAgICAgICAgPHN0eWxlIGpzeD57YFxyXG5cclxuICAgICAgICAgIC5kZXNjcmlwdGlvbiB7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIkFyaWFsXCI7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogXCIxMHB4XCI7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGxpIHtcclxuICAgICAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgICAgICAgbWFyZ2luOiA1cHggMDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgIGNvbG9yOiBibHVlO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGE6aG92ZXIge1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjY7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgYH08L3N0eWxlPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPC9MYXlvdXQ+XHJcbiAgICApO1xyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRmluZENvbXBuZW50O1xyXG4iXX0= */\n/*@ sourceURL=/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js */")));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FindCompnent);

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "bluebird":
/*!***************************!*\
  !*** external "bluebird" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("bluebird");

/***/ }),

/***/ "isomorphic-fetch":
/*!***********************************!*\
  !*** external "isomorphic-fetch" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-fetch");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map