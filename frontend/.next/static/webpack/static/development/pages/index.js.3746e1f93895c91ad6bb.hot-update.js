webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../lib/utils.js */ "./lib/utils.js");
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_lib_utils_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_Layout_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/Layout.js */ "./components/Layout.js");







var _jsxFileName = "/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;

__webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");




var textStyle = {
  fontFamily: "Arial"
};
var imgStyle = {
  height: "200px",
  marginTop: "20px"
};
var tableStyle = {
  margin: "0 auto",
  align: "center",
  border: "2px solid black"
};
var returnedTextStyle = {
  fontFamily: "Avantgarde, sans-serif"
};

var FindCompnent =
/*#__PURE__*/
function (_React$Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(FindCompnent, _React$Component);

  function FindCompnent(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, FindCompnent);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(FindCompnent).call(this, props));
    _this.state = {
      search: ""
    };
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(FindCompnent, [{
    key: "handleUpdate",
    value: function handleUpdate(evt) {
      this.setState({
        search: evt.target.value
      });
    }
  }, {
    key: "handleSearch",
    value: function () {
      var _handleSearch = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(evt) {
        var value;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.handleUpdate(evt);
                _context.next = 3;
                return Object(_lib_utils_js__WEBPACK_IMPORTED_MODULE_9__["getInfo"])(evt.target.value);

              case 3:
                value = _context.sent;
                this.setState({
                  value: value
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function handleSearch(_x) {
        return _handleSearch.apply(this, arguments);
      }

      return handleSearch;
    }()
  }, {
    key: "render",
    value: function render() {
      return __jsx(_components_Layout_js__WEBPACK_IMPORTED_MODULE_10__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }, __jsx("div", {
        style: {
          margin: "auto auto",
          width: "600px",
          textAlign: "center"
        },
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, __jsx("h2", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, "Food Search"), __jsx("img", {
        src: "/static/Bounty.png",
        alt: "someFood",
        style: imgStyle,
        className: "jsx-1455412518" + " " + "App-logo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }), __jsx("p", {
        style: textStyle,
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      }, __jsx("input", {
        type: "text",
        value: this.state.search,
        onChange: this.handleSearch.bind(this),
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      })), "value" in this.state && this.state.value.length > 0 ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      }, __jsx("br", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        },
        __self: this
      }), __jsx("table", {
        style: tableStyle,
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }, __jsx("thead", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        },
        __self: this
      }, __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        },
        __self: this
      }, "Description"), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 54
        },
        __self: this
      }, "KCal"), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        },
        __self: this
      }, "Protein"), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        },
        __self: this
      }, "Total Fat"), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }, "Carbs")), __jsx("tbody", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }, this.state.value.map(function (item) {
        return __jsx("tr", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 62
          },
          __self: this
        }, __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 63
          },
          __self: this
        }, item.description), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 64
          },
          __self: this
        }, item.kcal), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          },
          __self: this
        }, item.protein_g), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 66
          },
          __self: this
        }, (item.fa_sat_g + item.fa_mono_g + item.fa_poly_g).toFixed(2)), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 67
          },
          __self: this
        }, item.carbohydrate_g));
      })))) : null, __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default.a, {
        id: "1455412518",
        __self: this
      }, ".description.jsx-1455412518{font-family:\"Arial\";font-size:\"10px\";}ul.jsx-1455412518{padding:0;}li.jsx-1455412518{list-style:none;margin:5px 0;}a.jsx-1455412518{-webkit-text-decoration:none;text-decoration:none;color:blue;}a.jsx-1455412518:hover{opacity:0.6;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTJFb0IsQUFJaUMsQUFLVixBQUlNLEFBSUssQUFLVCxVQVpkLEVBYUEsSUFUZSxJQVRJLFNBVW5CLFFBVEEsYUFZYSxXQUNiIiwiZmlsZSI6Ii9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsicmVxdWlyZSAoXCJpc29tb3JwaGljLWZldGNoXCIpXHJcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtnZXRJbmZvfSBmcm9tICcuLi9saWIvdXRpbHMuanMnO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gJy4uL2NvbXBvbmVudHMvTGF5b3V0LmpzJztcclxuXHJcbmNvbnN0IHRleHRTdHlsZSA9IHtcclxuICBmb250RmFtaWx5OiBcIkFyaWFsXCJcclxufVxyXG5cclxuY29uc3QgaW1nU3R5bGUgPSB7XHJcbiAgaGVpZ2h0OiBcIjIwMHB4XCIsXHJcbiAgbWFyZ2luVG9wOiBcIjIwcHhcIlxyXG59XHJcblxyXG5jb25zdCB0YWJsZVN0eWxlID0ge1xyXG4gIG1hcmdpbjogXCIwIGF1dG9cIixcclxuICBhbGlnbjogXCJjZW50ZXJcIixcclxuICBib3JkZXI6IFwiMnB4IHNvbGlkIGJsYWNrXCJcclxuXHJcbn1cclxuXHJcbmNvbnN0IHJldHVybmVkVGV4dFN0eWxlID0ge1xyXG4gICAgZm9udEZhbWlseTogXCJBdmFudGdhcmRlLCBzYW5zLXNlcmlmXCJcclxufVxyXG5cclxuICBjbGFzcyBGaW5kQ29tcG5lbnQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgICB0aGlzLnN0YXRlPXtzZWFyY2g6IFwiXCJ9XHJcbiAgfVxyXG5cclxuICBoYW5kbGVVcGRhdGUoZXZ0KXtcclxuICAgIHRoaXMuc2V0U3RhdGUoe3NlYXJjaDogZXZ0LnRhcmdldC52YWx1ZX0pO1xyXG4gIH1cclxuXHJcbiAgYXN5bmMgaGFuZGxlU2VhcmNoKGV2dCkge1xyXG4gICAgdGhpcy5oYW5kbGVVcGRhdGUoZXZ0KTtcclxuICAgIGNvbnN0IHZhbHVlID0gYXdhaXQgZ2V0SW5mbyhldnQudGFyZ2V0LnZhbHVlKTtcclxuICAgIHRoaXMuc2V0U3RhdGUoe3ZhbHVlfSk7XHJcbiAgfVxyXG5cclxuICByZW5kZXIoKSB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8TGF5b3V0PlxyXG4gICAgICA8ZGl2IHN0eWxlPXt7IG1hcmdpbjogXCJhdXRvIGF1dG9cIiwgd2lkdGg6IFwiNjAwcHhcIiwgdGV4dEFsaWduOiBcImNlbnRlclwifX0+XHJcbiAgICAgICAgPGgyPkZvb2QgU2VhcmNoPC9oMj5cclxuICAgICAgICA8aW1nIHNyYz1cIi9zdGF0aWMvQm91bnR5LnBuZ1wiIGFsdD1cInNvbWVGb29kXCIgY2xhc3NOYW1lPVwiQXBwLWxvZ29cIiBzdHlsZT17aW1nU3R5bGV9Lz5cclxuICAgICAgICA8cCBzdHlsZT17dGV4dFN0eWxlfT48aW5wdXQgdHlwZT0ndGV4dCcgdmFsdWU9e3RoaXMuc3RhdGUuc2VhcmNofSBvbkNoYW5nZT17dGhpcy5oYW5kbGVTZWFyY2guYmluZCh0aGlzKX0gLz48L3A+XHJcbiAgICAgICAgeyhcInZhbHVlXCIgaW4gdGhpcy5zdGF0ZSAmJiB0aGlzLnN0YXRlLnZhbHVlLmxlbmd0aCA+IDApID8gPGRpdj5cclxuICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgIDx0YWJsZSBzdHlsZT17dGFibGVTdHlsZX0+XHJcbiAgICAgICAgICAgIDx0aGVhZD5cclxuICAgICAgICAgICAgPHRoPkRlc2NyaXB0aW9uPC90aD5cclxuICAgICAgICAgICAgPHRoPktDYWw8L3RoPlxyXG4gICAgICAgICAgICA8dGg+UHJvdGVpbjwvdGg+XHJcbiAgICAgICAgICAgIDx0aD5Ub3RhbCBGYXQ8L3RoPlxyXG4gICAgICAgICAgICA8dGg+Q2FyYnM8L3RoPlxyXG4gICAgICAgICAgICA8L3RoZWFkPlxyXG4gICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgIHt0aGlzLnN0YXRlLnZhbHVlLm1hcChmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuKFxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgPHRkPntpdGVtLmRlc2NyaXB0aW9ufTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2l0ZW0ua2NhbH08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPntpdGVtLnByb3RlaW5fZ308L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPnsoaXRlbS5mYV9zYXRfZyArIGl0ZW0uZmFfbW9ub19nICsgaXRlbS5mYV9wb2x5X2cpLnRvRml4ZWQoMil9PC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZD57aXRlbS5jYXJib2h5ZHJhdGVfZ308L3RkPlxyXG4gICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgPC90Ym9keT5cclxuICAgICAgICAgICAgPC90YWJsZT5cclxuICAgICAgICAgIDwvZGl2PiA6IG51bGx9XHJcblxyXG4gICAgICAgIDxzdHlsZSBqc3g+e2BcclxuXHJcbiAgICAgICAgICAuZGVzY3JpcHRpb24ge1xyXG4gICAgICAgICAgICBmb250LWZhbWlseTogXCJBcmlhbFwiO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IFwiMTBweFwiO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHVsIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICBjb2xvcjogYmx1ZTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBhOmhvdmVyIHtcclxuICAgICAgICAgICAgb3BhY2l0eTogMC42O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIGB9PC9zdHlsZT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDwvTGF5b3V0PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEZpbmRDb21wbmVudDtcclxuIl19 */\n/*@ sourceURL=/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js */")));
    }
  }]);

  return FindCompnent;
}(react__WEBPACK_IMPORTED_MODULE_8___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (FindCompnent);

/***/ })

})
//# sourceMappingURL=index.js.3746e1f93895c91ad6bb.hot-update.js.map