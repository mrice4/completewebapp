webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../lib/utils.js */ "./lib/utils.js");
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_lib_utils_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _components_Layout_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/Layout.js */ "./components/Layout.js");








var _jsxFileName = "/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement;

__webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");




var textStyle = {
  fontFamily: "Arial"
};
var imgStyle = {
  height: "80px",
  marginTop: "20px"
};
var returnedTextStyle = {
  fontFamily: "Avantgarde, sans-serif"
};

var FindCompnent =
/*#__PURE__*/
function (_React$Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(FindCompnent, _React$Component);

  function FindCompnent(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, FindCompnent);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(FindCompnent).call(this, props));
    _this.state = {
      search: ""
    };
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(FindCompnent, [{
    key: "handleUpdate",
    value: function handleUpdate(evt) {
      this.setState({
        search: evt.target.value
      });
    }
  }, {
    key: "handleSearch",
    value: function () {
      var _handleSearch = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(evt) {
        var value;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.handleUpdate(evt);
                _context.next = 3;
                return Object(_lib_utils_js__WEBPACK_IMPORTED_MODULE_10__["getInfo"])(evt.target.value);

              case 3:
                value = _context.sent;
                this.setState({
                  value: value
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function handleSearch(_x) {
        return _handleSearch.apply(this, arguments);
      }

      return handleSearch;
    }()
  }, {
    key: "render",
    value: function render() {
      var _jsx;

      return __jsx(_components_Layout_js__WEBPACK_IMPORTED_MODULE_11__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      }, __jsx("div", {
        style: {
          margin: "auto auto",
          width: "600px",
          textAlign: "center"
        },
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }, __jsx("h2", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, "Food Search"), __jsx("p", {
        style: textStyle,
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }, __jsx("input", {
        type: "text",
        value: this.state.search,
        onChange: this.handleSearch.bind(this),
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      })), "value" in this.state && this.state.value.length > 0 ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }, __jsx("img", (_jsx = {
        style: returnedTextStyle,
        src: "/static/Bounty.png"
      }, Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_jsx, "style", foundImgStyle), Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_jsx, "className", "jsx-1455412518" + " " + "foundImg"), Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_jsx, "__source", {
        fileName: _jsxFileName,
        lineNumber: 42
      }), Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_jsx, "__self", this), _jsx)), " ", __jsx("br", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        },
        __self: this
      }), __jsx("table", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }, __jsx("thead", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, "Description"), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, "Total Fat "), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      }, "Protein")), __jsx("tbody", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        },
        __self: this
      }, this.state.value.map(function (item) {
        return __jsx("tr", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 53
          },
          __self: this
        }, __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 54
          },
          __self: this
        }, item.description), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 55
          },
          __self: this
        }, (item.fa_sat_g + item.fa_mono_g + item.fa_poly_g).toFixed(2)), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 56
          },
          __self: this
        }, item.protein_g));
      })))) : null, __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_8___default.a, {
        id: "1455412518",
        __self: this
      }, ".description.jsx-1455412518{font-family:\"Arial\";font-size:\"10px\";}ul.jsx-1455412518{padding:0;}li.jsx-1455412518{list-style:none;margin:5px 0;}a.jsx-1455412518{-webkit-text-decoration:none;text-decoration:none;color:blue;}a.jsx-1455412518:hover{opacity:0.6;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWdFb0IsQUFJaUMsQUFLVixBQUlNLEFBSUssQUFLVCxVQVpkLEVBYUEsSUFUZSxJQVRJLFNBVW5CLFFBVEEsYUFZYSxXQUNiIiwiZmlsZSI6Ii9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsicmVxdWlyZSAoXCJpc29tb3JwaGljLWZldGNoXCIpXHJcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtnZXRJbmZvfSBmcm9tICcuLi9saWIvdXRpbHMuanMnO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gJy4uL2NvbXBvbmVudHMvTGF5b3V0LmpzJztcclxuXHJcbmNvbnN0IHRleHRTdHlsZSA9IHtcclxuICBmb250RmFtaWx5OiBcIkFyaWFsXCJcclxufVxyXG5cclxuY29uc3QgaW1nU3R5bGUgPSB7XHJcbiAgaGVpZ2h0OiBcIjgwcHhcIixcclxuICBtYXJnaW5Ub3A6IFwiMjBweFwiXHJcbn1cclxuXHJcbmNvbnN0IHJldHVybmVkVGV4dFN0eWxlID0ge1xyXG4gICAgZm9udEZhbWlseTogXCJBdmFudGdhcmRlLCBzYW5zLXNlcmlmXCJcclxufVxyXG5cclxuICBjbGFzcyBGaW5kQ29tcG5lbnQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgICB0aGlzLnN0YXRlPXtzZWFyY2g6IFwiXCJ9XHJcbiAgfVxyXG5cclxuICBoYW5kbGVVcGRhdGUoZXZ0KXtcclxuICAgIHRoaXMuc2V0U3RhdGUoe3NlYXJjaDogZXZ0LnRhcmdldC52YWx1ZX0pO1xyXG4gIH1cclxuXHJcbiAgYXN5bmMgaGFuZGxlU2VhcmNoKGV2dCkge1xyXG4gICAgdGhpcy5oYW5kbGVVcGRhdGUoZXZ0KTtcclxuICAgIGNvbnN0IHZhbHVlID0gYXdhaXQgZ2V0SW5mbyhldnQudGFyZ2V0LnZhbHVlKTtcclxuICAgIHRoaXMuc2V0U3RhdGUoe3ZhbHVlfSk7XHJcbiAgfVxyXG5cclxuICByZW5kZXIoKSB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8TGF5b3V0PlxyXG4gICAgICA8ZGl2IHN0eWxlPXt7IG1hcmdpbjogXCJhdXRvIGF1dG9cIiwgd2lkdGg6IFwiNjAwcHhcIiwgdGV4dEFsaWduOiBcImNlbnRlclwifX0+XHJcbiAgICAgICAgPGgyPkZvb2QgU2VhcmNoPC9oMj5cclxuICAgICAgICA8cCBzdHlsZT17dGV4dFN0eWxlfT48aW5wdXQgdHlwZT0ndGV4dCcgdmFsdWU9e3RoaXMuc3RhdGUuc2VhcmNofSBvbkNoYW5nZT17dGhpcy5oYW5kbGVTZWFyY2guYmluZCh0aGlzKX0gLz48L3A+XHJcbiAgICAgICAgeyhcInZhbHVlXCIgaW4gdGhpcy5zdGF0ZSAmJiB0aGlzLnN0YXRlLnZhbHVlLmxlbmd0aCA+IDApID8gPGRpdj5cclxuICAgICAgICA8aW1nIHN0eWxlPXtyZXR1cm5lZFRleHRTdHlsZX0gc3JjPXtcIi9zdGF0aWMvQm91bnR5LnBuZ1wifVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJmb3VuZEltZ1wiIHN0eWxlPXtmb3VuZEltZ1N0eWxlfS8+IDxiciAvPlxyXG4gICAgICAgICAgICA8dGFibGU+XHJcbiAgICAgICAgICAgIDx0aGVhZD5cclxuICAgICAgICAgICAgPHRoPkRlc2NyaXB0aW9uPC90aD5cclxuICAgICAgICAgICAgPHRoPlRvdGFsIEZhdCA8L3RoPlxyXG4gICAgICAgICAgICA8dGg+UHJvdGVpbjwvdGg+XHJcbiAgICAgICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgICAgIDx0Ym9keT5cclxuICAgICAgICAgICAge3RoaXMuc3RhdGUudmFsdWUubWFwKGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAgICAgICByZXR1cm4oXHJcbiAgICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2l0ZW0uZGVzY3JpcHRpb259PC90ZD5cclxuICAgICAgICAgICAgICAgIDx0ZD57KGl0ZW0uZmFfc2F0X2cgKyBpdGVtLmZhX21vbm9fZyArIGl0ZW0uZmFfcG9seV9nKS50b0ZpeGVkKDIpfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+e2l0ZW0ucHJvdGVpbl9nfTwvdGQ+XHJcbiAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICA8L3RhYmxlPlxyXG4gICAgICAgICAgPC9kaXY+IDogbnVsbH1cclxuXHJcbiAgICAgICAgPHN0eWxlIGpzeD57YFxyXG5cclxuICAgICAgICAgIC5kZXNjcmlwdGlvbiB7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIkFyaWFsXCI7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogXCIxMHB4XCI7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgdWwge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGxpIHtcclxuICAgICAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgICAgICAgbWFyZ2luOiA1cHggMDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgIGNvbG9yOiBibHVlO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGE6aG92ZXIge1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjY7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgYH08L3N0eWxlPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPC9MYXlvdXQ+XHJcbiAgICApO1xyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRmluZENvbXBuZW50O1xyXG4iXX0= */\n/*@ sourceURL=/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js */")));
    }
  }]);

  return FindCompnent;
}(react__WEBPACK_IMPORTED_MODULE_9___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (FindCompnent);

/***/ })

})
//# sourceMappingURL=index.js.2fd1034e0ba66e129dc4.hot-update.js.map