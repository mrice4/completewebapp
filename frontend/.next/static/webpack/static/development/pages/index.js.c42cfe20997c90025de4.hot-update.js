webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../lib/utils.js */ "./lib/utils.js");
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_lib_utils_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_Layout_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/Layout.js */ "./components/Layout.js");







var _jsxFileName = "/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;

__webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");




var textStyle = {
  fontFamily: "Arial"
};
var imgStyle = {
  height: "200px",
  marginTop: "20px"
};
var returnedTextStyle = {
  fontFamily: "Avantgarde, sans-serif"
};

var FindCompnent =
/*#__PURE__*/
function (_React$Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(FindCompnent, _React$Component);

  function FindCompnent(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, FindCompnent);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(FindCompnent).call(this, props));
    _this.state = {
      search: ""
    };
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(FindCompnent, [{
    key: "handleUpdate",
    value: function handleUpdate(evt) {
      this.setState({
        search: evt.target.value
      });
    }
  }, {
    key: "handleSearch",
    value: function () {
      var _handleSearch = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(evt) {
        var value;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.handleUpdate(evt);
                _context.next = 3;
                return Object(_lib_utils_js__WEBPACK_IMPORTED_MODULE_9__["getInfo"])(evt.target.value);

              case 3:
                value = _context.sent;
                this.setState({
                  value: value
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function handleSearch(_x) {
        return _handleSearch.apply(this, arguments);
      }

      return handleSearch;
    }()
  }, {
    key: "render",
    value: function render() {
      return __jsx(_components_Layout_js__WEBPACK_IMPORTED_MODULE_10__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      }, __jsx("div", {
        style: {
          margin: "auto auto",
          width: "600px",
          textAlign: "center"
        },
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }, __jsx("h2", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, "Food Search"), __jsx("img", {
        src: "/static/Bounty.png",
        alt: "someFood",
        style: imgStyle,
        className: "jsx-1455412518" + " " + "App-logo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }), __jsx("p", {
        style: textStyle,
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }, __jsx("input", {
        type: "text",
        value: this.state.search,
        onChange: this.handleSearch.bind(this),
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      })), "value" in this.state && this.state.value.length > 0 ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      }, "className=\"foundImg\"/>", __jsx("br", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }), __jsx("table", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, __jsx("thead", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, "Description"), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      }, "Total Fat "), __jsx("th", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      }, "Protein")), __jsx("tbody", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }, this.state.value.map(function (item) {
        return __jsx("tr", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 54
          },
          __self: this
        }, __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 55
          },
          __self: this
        }, item.description), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 56
          },
          __self: this
        }, (item.fa_sat_g + item.fa_mono_g + item.fa_poly_g).toFixed(2)), __jsx("td", {
          className: "jsx-1455412518",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 57
          },
          __self: this
        }, item.protein_g));
      })))) : null, __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default.a, {
        id: "1455412518",
        __self: this
      }, ".description.jsx-1455412518{font-family:\"Arial\";font-size:\"10px\";}ul.jsx-1455412518{padding:0;}li.jsx-1455412518{list-style:none;margin:5px 0;}a.jsx-1455412518{-webkit-text-decoration:none;text-decoration:none;color:blue;}a.jsx-1455412518:hover{opacity:0.6;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWlFb0IsQUFJaUMsQUFLVixBQUlNLEFBSUssQUFLVCxVQVpkLEVBYUEsSUFUZSxJQVRJLFNBVW5CLFFBVEEsYUFZYSxXQUNiIiwiZmlsZSI6Ii9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL0NvbXBsZXRlV2ViQXBwL2Zyb250ZW5kL3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsicmVxdWlyZSAoXCJpc29tb3JwaGljLWZldGNoXCIpXHJcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtnZXRJbmZvfSBmcm9tICcuLi9saWIvdXRpbHMuanMnO1xyXG5pbXBvcnQgTGF5b3V0IGZyb20gJy4uL2NvbXBvbmVudHMvTGF5b3V0LmpzJztcclxuXHJcbmNvbnN0IHRleHRTdHlsZSA9IHtcclxuICBmb250RmFtaWx5OiBcIkFyaWFsXCJcclxufVxyXG5cclxuY29uc3QgaW1nU3R5bGUgPSB7XHJcbiAgaGVpZ2h0OiBcIjIwMHB4XCIsXHJcbiAgbWFyZ2luVG9wOiBcIjIwcHhcIlxyXG59XHJcblxyXG5jb25zdCByZXR1cm5lZFRleHRTdHlsZSA9IHtcclxuICAgIGZvbnRGYW1pbHk6IFwiQXZhbnRnYXJkZSwgc2Fucy1zZXJpZlwiXHJcbn1cclxuXHJcbiAgY2xhc3MgRmluZENvbXBuZW50IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgIHN1cGVyKHByb3BzKTtcclxuICAgICAgdGhpcy5zdGF0ZT17c2VhcmNoOiBcIlwifVxyXG4gIH1cclxuXHJcbiAgaGFuZGxlVXBkYXRlKGV2dCl7XHJcbiAgICB0aGlzLnNldFN0YXRlKHtzZWFyY2g6IGV2dC50YXJnZXQudmFsdWV9KTtcclxuICB9XHJcblxyXG4gIGFzeW5jIGhhbmRsZVNlYXJjaChldnQpIHtcclxuICAgIHRoaXMuaGFuZGxlVXBkYXRlKGV2dCk7XHJcbiAgICBjb25zdCB2YWx1ZSA9IGF3YWl0IGdldEluZm8oZXZ0LnRhcmdldC52YWx1ZSk7XHJcbiAgICB0aGlzLnNldFN0YXRlKHt2YWx1ZX0pO1xyXG4gIH1cclxuXHJcbiAgcmVuZGVyKCkge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgPExheW91dD5cclxuICAgICAgPGRpdiBzdHlsZT17eyBtYXJnaW46IFwiYXV0byBhdXRvXCIsIHdpZHRoOiBcIjYwMHB4XCIsIHRleHRBbGlnbjogXCJjZW50ZXJcIn19PlxyXG4gICAgICAgIDxoMj5Gb29kIFNlYXJjaDwvaDI+XHJcbiAgICAgICAgPGltZyBzcmM9XCIvc3RhdGljL0JvdW50eS5wbmdcIiBhbHQ9XCJzb21lRm9vZFwiIGNsYXNzTmFtZT1cIkFwcC1sb2dvXCIgc3R5bGU9e2ltZ1N0eWxlfS8+XHJcbiAgICAgICAgPHAgc3R5bGU9e3RleHRTdHlsZX0+PGlucHV0IHR5cGU9J3RleHQnIHZhbHVlPXt0aGlzLnN0YXRlLnNlYXJjaH0gb25DaGFuZ2U9e3RoaXMuaGFuZGxlU2VhcmNoLmJpbmQodGhpcyl9IC8+PC9wPlxyXG4gICAgICAgIHsoXCJ2YWx1ZVwiIGluIHRoaXMuc3RhdGUgJiYgdGhpcy5zdGF0ZS52YWx1ZS5sZW5ndGggPiAwKSA/IDxkaXY+XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImZvdW5kSW1nXCIvPlxyXG4gICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgPHRhYmxlPlxyXG4gICAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgIDx0aD5EZXNjcmlwdGlvbjwvdGg+XHJcbiAgICAgICAgICAgIDx0aD5Ub3RhbCBGYXQgPC90aD5cclxuICAgICAgICAgICAgPHRoPlByb3RlaW48L3RoPlxyXG4gICAgICAgICAgICA8L3RoZWFkPlxyXG4gICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgIHt0aGlzLnN0YXRlLnZhbHVlLm1hcChmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuKFxyXG4gICAgICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgICAgPHRkPntpdGVtLmRlc2NyaXB0aW9ufTwvdGQ+XHJcbiAgICAgICAgICAgICAgICA8dGQ+eyhpdGVtLmZhX3NhdF9nICsgaXRlbS5mYV9tb25vX2cgKyBpdGVtLmZhX3BvbHlfZykudG9GaXhlZCgyKX08L3RkPlxyXG4gICAgICAgICAgICAgICAgPHRkPntpdGVtLnByb3RlaW5fZ308L3RkPlxyXG4gICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgPC90Ym9keT5cclxuICAgICAgICAgICAgPC90YWJsZT5cclxuICAgICAgICAgIDwvZGl2PiA6IG51bGx9XHJcblxyXG4gICAgICAgIDxzdHlsZSBqc3g+e2BcclxuXHJcbiAgICAgICAgICAuZGVzY3JpcHRpb24ge1xyXG4gICAgICAgICAgICBmb250LWZhbWlseTogXCJBcmlhbFwiO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IFwiMTBweFwiO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHVsIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICBjb2xvcjogYmx1ZTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBhOmhvdmVyIHtcclxuICAgICAgICAgICAgb3BhY2l0eTogMC42O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIGB9PC9zdHlsZT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDwvTGF5b3V0PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEZpbmRDb21wbmVudDtcclxuIl19 */\n/*@ sourceURL=/mnt/c/Users/devlk/OneDrive/Documents/Databases/CompleteWebApp/frontend/pages/index.js */")));
    }
  }]);

  return FindCompnent;
}(react__WEBPACK_IMPORTED_MODULE_8___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (FindCompnent);

/***/ })

})
//# sourceMappingURL=index.js.c42cfe20997c90025de4.hot-update.js.map